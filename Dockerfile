# Stage 0, "build-stage", based on Node.js, to build and compile the frontend
FROM bayesimpact/react-base as build-stage

COPY code/ /app/

WORKDIR /app
RUN yarn install
RUN npm run build

# Stage 1, based on Nginx, to have only the compiled app, ready for production with Nginx
FROM nginx:1.15.8-alpine
COPY --from=build-stage /app/build/ /usr/share/nginx/html
