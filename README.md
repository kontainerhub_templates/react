# REACT STACK

## Template React
Esta stack prove uma aplicação com o framework `React` e um servidor web `nginx`.

## Código
Todo código referente ao comportamento da aplicação está dentro do diretório `code/`

## Atualização/Instalação
Para instalação de dependências ao projeto ou alteração dos passos de build, todas os passos podem ser descritos dentro do arquivo Dockerfile.